﻿using System;
namespace assignment2
{
    public class CurrentAccount : Account
    {
        readonly double OVERDRAFT_LIMIT = 200000;

        public CurrentAccount(String accountNum, String holderName, double balance) : base(accountNum, holderName, balance)
        {
        }

        public override void Withdraw(double amount)
        {
            base.Withdraw(amount);
            if(amount > balance && amount < OVERDRAFT_LIMIT)
            {
                Console.WriteLine("Ksh {0} withdrawn from Current Account", amount);
                balance -= amount;
            }
            else if(balance > amount)
            {
                Console.WriteLine("Ksh {0} withdrawn from Current Account", amount);
                balance -= amount;
            }
            else
            {
                Console.WriteLine("Unable to withdraw amount. Kindly try a lower amount");    
            }
        }

    }
}
