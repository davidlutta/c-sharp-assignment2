﻿using System;
namespace assignment2
{
    public class Account
    {
        protected String accountNumber, holderName;
        protected double balance;

        public Account(String accNum, String holderName, double balance)
        {
            this.accountNumber = accNum;
            this.holderName = holderName;
            this.balance = balance;
        }

        public virtual void Deposit(double depositAmt)
        {
            balance += depositAmt;
            Console.WriteLine("Amount deposited\nNew Balance is: {0}", balance);
            Console.WriteLine();
        }

        public double CheckBalance()
        {
            return balance;
        }

        public virtual void Withdraw(double amount)
        {

        }
        
    }
}
