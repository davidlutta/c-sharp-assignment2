﻿using System;
namespace assignment2
{
    public class SavingsAccount : Account
    {
        public SavingsAccount(String accountNumber, String holderName, double balance) : base(accountNumber, holderName, balance)
        {

        }
        public override void Withdraw(double amount)
        {
            base.Withdraw(amount);

            if(balance > amount)
            {
                Console.WriteLine("Ksh.{0} withdrawn from Savings Account", amount);
                balance -= amount;               
            }
            else
            {
                Console.WriteLine("Unable to withdraw amount. Kindly try a lower amount");
            }
        }

        public void AddInterest(double rate)
        {
            double amount = (balance * Math.Pow((1+((rate/100)/1)), (1*1)));
            balance = amount;
            Console.WriteLine("Interest added!\nNew Account balance: {0}", balance);
        }
    }
}
