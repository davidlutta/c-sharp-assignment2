﻿using System;

namespace assignment2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Question 2 Main Driver Code
            Console.Write("How many books do you have in your library? ");
            int Num = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine();


            Library[] books = new Library[Num];

            for(int i = 0; i < Num; i++){
                books[i] = new Library();
                Console.Write("Enter Name of Book {0}: ", (i+1));
                books[i].Name = Console.ReadLine();

                Console.Write("Enter the Code for {0}: ",books[i].Name);                
                books[i].Code = Console.ReadLine();
                Console.WriteLine();
            }
            
            foreach (Library book in books)
            {
                Console.WriteLine("{0}\t{1}", book.Code,book.Name);
            }

            //Question 1 Main Driver Code
            Console.Write("Enter Buying Price of Item: ");
            double BuyingPrice = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter Transport Price of Item: ");
            double TransportPrice = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter Selling Price of Item: ");
            double SellingPrice = Convert.ToDouble(Console.ReadLine());

            Business business = new Business(BuyingPrice, TransportPrice, SellingPrice);
            business.ComputeProfitOrLoss();
            Console.ReadKey();


            // Question 3 Main Driver Code
            Account savingsAccount = new SavingsAccount("sswrf133dsc32f", "Kasuku Center", 2000.00);
            savingsAccount.Deposit(100);
            Console.WriteLine();
            savingsAccount.Withdraw(132000);

            Account currentAccount = new CurrentAccount("ccpoermIXSorr34", "Yaya Center", 1000.00);
            currentAccount.Deposit(20000);
            Console.WriteLine();
            currentAccount.Withdraw(40.00);
            Console.WriteLine("New balance is: {0}", currentAccount.CheckBalance());
        }
    }
} 